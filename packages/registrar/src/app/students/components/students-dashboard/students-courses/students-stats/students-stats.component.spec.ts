import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsStatsComponent } from './students-stats.component';

describe('StudentsStatsComponent', () => {
  let component: StudentsStatsComponent;
  let fixture: ComponentFixture<StudentsStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
