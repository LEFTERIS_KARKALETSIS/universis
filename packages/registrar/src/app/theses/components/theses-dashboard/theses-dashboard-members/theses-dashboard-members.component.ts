import {Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';
// tslint:disable-next-line:max-line-length
import {AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult} from '@universis/ngx-tables';
import {DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import {Subscription, combineLatest} from 'rxjs';
import {ActivatedTableService} from '@universis/ngx-tables';
import { TableConfigurationResolver } from '../../../../registrar-shared/table-configuration.resolvers';
import { map } from 'rxjs/operators';



@Component({
  selector: 'app-theses-dashboard-members',
  templateUrl: './theses-dashboard-members.component.html',
})
export class ThesesDashboardMembersComponent implements OnInit, OnDestroy {

  public model: any;
  private dataSubscription: Subscription;
  private subscription: Subscription;

  @ViewChild('members') members: AdvancedTableComponent;
  public thesesID: any;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _context: AngularDataContext,
              private _resolver: TableConfigurationResolver,
              private _loading: LoadingService) {}

  async ngOnInit() {

    this.subscription = combineLatest(
      this._activatedRoute.params,
      this._resolver.get('StudentTheses', 'members')
      ).pipe(
        map(([params, tableConfiguration]) => ({params, tableConfiguration}))
      ).subscribe(async (results) => {
      this._activatedTable.activeTable = this.members;
      this.thesesID = results.params.id;
      this.model = await this._context.model('Theses')
        .where('id').equal(this.thesesID)
        .expand('instructor,type,status,students($expand=student($expand=department,person))')
        .getItem();
      this.members.query = this._context.model('ThesisRoles')
        .where('thesis').equal(this.thesesID)
        .expand('member($expand=department)')
        .prepare();
  
      this.members.config = AdvancedTableConfiguration.cast(results.tableConfiguration);
      // declare model for advance search filter criteria
      this.members.config.model = 'ThesisRoles' ;
      this.members.fetch();

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe( fragment => {
        if (fragment && fragment === 'reload') {
          this.members.fetch(true);
        }
      });
      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
          this.members.config = data.tableConfiguration;
          this.members.ngOnInit();
        }
      });
    });
    
  }
  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;

  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  remove() {
    if (this.members && this.members.selected && this.members.selected.length) {/*
      this.paths.filter( x => {
        return x.show === true;
      }).*/
      const items = this.members.selected.filter( x => {
        return x.roleName !== 'supervisor';
      }).map( member => {
        return {
          thesis: this.thesesID,
          member: member
        };
      });
      if (items.length > 0) {
      return this._modalService.showWarningDialog(
        this._translateService.instant('Theses.RemoveMemberTitle'),
        this._translateService.instant('Theses.RemoveMemberMessage'),
        DIALOG_BUTTONS.OkCancel).then( result => {
        if (result === 'ok') {
          this._loading.showLoading();
          this._context.model('ThesisRoles').remove(items).then( () => {
            this._toastService.show(
              this._translateService.instant('Theses.RemoveMembersMessage.title'),
              this._translateService.instant((items.length === 1 ?
                'Theses.RemoveMembersMessage.one' : 'Theses.RemoveMembersMessage.many')
                , { value: items.length })
            );
            this.members.fetch(true);
            this._loading.hideLoading();
          }).catch( err => {
            this._loading.hideLoading();
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });
      } else {
        return this._modalService.showDialog(
          this._translateService.instant('Theses.RemoveSupervisorTitle'),
          this._translateService.instant('Theses.RemoveSupervisorMessage'),
          DIALOG_BUTTONS.Ok).then( result => {
            this.members.fetch(true);
        });
          }
      }
    }
}

