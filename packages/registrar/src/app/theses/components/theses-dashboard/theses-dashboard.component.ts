import { Component, OnDestroy, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { UserActivityService} from '@universis/common';

@Component({
  selector: 'app-theses-dashboard',
  templateUrl: './theses-dashboard.component.html',
})
export class ThesesDashboardComponent implements OnInit, OnDestroy {

  public model: any;
  private subscription: Subscription;
  public isLoading = true;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {}

  async ngOnInit() {
    this.isLoading = true;
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('Theses')
        .where('id').equal(params.id)
        .expand('instructor,type,status,students($expand=student($expand=department,person))')
        .getItem();
      this.isLoading = false;
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
