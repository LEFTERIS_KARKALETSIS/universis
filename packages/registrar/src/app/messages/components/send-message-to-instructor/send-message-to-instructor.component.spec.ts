import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendMessageToInstructorComponent } from './send-message-to-instructor.component';

describe('SendMessageToInstructorComponent', () => {
  let component: SendMessageToInstructorComponent;
  let fixture: ComponentFixture<SendMessageToInstructorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendMessageToInstructorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendMessageToInstructorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
