import { Component, OnInit, OnDestroy, Input, EventEmitter, ViewChild, ElementRef, Output, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import 'rxjs/add/observable/combineLatest';
import { ErrorService, ModalService, ToastService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { AppEventService } from '@universis/common';
import { Observable, Subscription } from 'rxjs';

declare var $: any;

@Component({
  selector: 'app-register-send-message-action',
  templateUrl: './send-message-action.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
        .fa-1_5x {
            font-size: 1.5rem;
        }
        app-compose-message > .card { box-shadow: none !important; }
        `

  ]
})
export class SendMessageActionComponent extends RouterModalOkCancel implements OnInit, OnDestroy {

  @Input() items: any;
  @Input() modalIcon = 'fas fa-project-diagram';
  @Input() description: string;
  @Output() message: any = {};
  public messageChange: any = new EventEmitter<any>();
  public lastError: any;
  public loading = false;
  @ViewChild('progress') progress: ElementRef;
  @Input() refresh: any = new EventEmitter<any>();
  @Input() execute: Observable<any>;
  private refreshSubscription: Subscription;
  private executeSubscription: Subscription;

  constructor(router: Router,
    activatedRoute: ActivatedRoute,
    private _translateService: TranslateService,
    private _context: AngularDataContext,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _appEvent: AppEventService,
    private _toastService: ToastService) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-lg';
    // set modal title
    this.modalTitle = 'Requests.Edit.Label';
    // disable ok button
    this.okButtonDisabled = false;
    // set button text
    this.okButtonText = 'Requests.Edit.Start';
  }

  ngOnInit() {
    this.refreshSubscription = this.refresh.subscribe((value) => {
      if (value && value.progress) {
        let progress = 0;
        if (value.progress < 0) {
          progress = 0;
        } else if (value.progress > 100) {
          progress = 100;
        } else {
          progress = value.progress;
        }
        $(this.progress.nativeElement).find('.progress-bar').css('width', `${progress}%`);
        this.showProgress();
      }
    });
  }

  showProgress() {
    $(this.progress.nativeElement).css('visibility', 'visible');
  }

  hideProgress() {
    $(this.progress.nativeElement).css('visibility', 'hidden');
  }

  ngOnDestroy() {
    if (this.refreshSubscription) {
      this.refreshSubscription.unsubscribe();
    }
    if (this.executeSubscription) {
      this.executeSubscription.unsubscribe();
    }
  }

  cancel(): Promise<any> {
    if (this.loading) {
      return;
    }
    // close
    if (this._modalService.modalRef) {
      return this._modalService.modalRef.hide();
    }
  }

  async ok() {
    try {
      this.loading = true;
      this.lastError = null;
      // do action
      this.execute.subscribe((result) => {
        this.loading = false;
        this.refresh.emit({
          progress: 100
        });
        if (result && result.errors && result.errors > 0) {
          // show message for partial success
          if (result.errors === 1) {
            this.lastError = new Error(this._translateService.instant(
              'Requests.Edit.CompletedWithErrors.Description.One'));
          } else {
            this.lastError = new Error(this._translateService.instant(
              'Requests.Edit.CompletedWithErrors.Description.Many',
              result));
          }
          this.refresh.emit({
            progress: 1
          });
          this.hideProgress();
          return;
        }
        if (this._modalService.modalRef) {
          this._modalService.modalRef.hide();
        }
      }, (err) => {
        this.loading = false;
        this.lastError = err;
        this.refresh.emit({
          progress: 0
        });
      });
    } catch (err) {
      this.loading = false;
      this.lastError = err;
    }
  }


}
