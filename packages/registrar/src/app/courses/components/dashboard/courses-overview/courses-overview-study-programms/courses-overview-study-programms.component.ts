import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-courses-overview-study-programms',
  templateUrl: './courses-overview-study-programms.component.html',
  styleUrls: ['./courses-overview-study-programms.component.scss']
})
export class CoursesOverviewStudyProgrammsComponent implements OnInit, OnDestroy {
  public courseId: any;
  public courses: any;
  private subscription: Subscription;

  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.courses = await this._context.model('SpecializationCourses')
        .where('studyProgramCourse/course').equal(params.id)
        .expand('specialization,semester,courseType,studyProgramCourse($expand=course,studyProgram($expand=department,studyLevel))')
        .take(-1)
        .orderByDescending('studyProgramCourse/studyProgram')
        .thenBy('specialization/specialty')
        .getItems();
      this.courseId = params.id;
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
