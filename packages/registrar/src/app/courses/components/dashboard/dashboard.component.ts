import {Component, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  public model: any;
  public tabs: any[];
  public isLoading = true;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.isLoading = true;
      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.tabs = this._activatedRoute.routeConfig.children.filter(route => typeof route.redirectTo === 'undefined');
      this.model = await this._context.model('Courses')
        .where('id').equal(params.id)
        .select('id', 'name', 'displayCode', 'courseStructureType')
        .getItem();
        this.isLoading = false;
      if  (this.model.courseStructureType === 4) {
        this.tabs = this.tabs.filter(x => {
        if (x.path !== 'exams' && x.path !== 'classes') {
          return x;
        }
      });
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
