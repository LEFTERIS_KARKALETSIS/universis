import { AfterViewInit, Component, Input, OnDestroy, ViewChild } from '@angular/core';
import { RouterModalOkCancel } from '@universis/common/routing';
import { ActivatedRoute, Router } from '@angular/router';
import { AppEventService } from '@universis/common';
import { LoadingService, ModalService, ToastService } from '@universis/common';
import { AdvancedFormComponent, EmptyValuePostProcessor } from '@universis/forms';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-add-department-role',
  templateUrl: './add-department-role.component.html'
})
export class AddDepartmentRoleComponent extends RouterModalOkCancel implements AfterViewInit, OnDestroy {
  public lastError: any;
  @ViewChild('formComponent') formComponent: AdvancedFormComponent;
  @Input() data: any;

  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    private readonly loadingService: LoadingService,
    private readonly toastService: ToastService,
    private readonly context: AngularDataContext,
    private readonly modalService: ModalService,
    private readonly translateService: TranslateService,
    private readonly appEvent: AppEventService
  ) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-lg';
    // set modal title
    this.modalTitle = 'Departments.Roles.Add.TitleLong';
    this.okButtonDisabled = true;
  }

  ngAfterViewInit() {
    // append collected data to form component
    this.formComponent.data = this.data;
  }
  ngOnDestroy(): void {}

  cancel(): Promise<any> {
    // close
    if (this.modalService.modalRef) {
      return this.modalService.modalRef.hide();
    }
  }

  async ok() {
    try {
      // reset last error
      this.lastError = null;
      this.loadingService.showLoading();
      // get data from component
      const departmentRole =
        this.formComponent && this.formComponent.form && this.formComponent.form.formio && this.formComponent.form.formio.data;
      if (departmentRole == null) {
        return this.cancel();
      }
      // parse empty values
      new EmptyValuePostProcessor().parse(this.formComponent.formConfig, departmentRole);
      // format roleName
      departmentRole.roleName = departmentRole.roleName && departmentRole.roleName.alternateName;
      // save a new department role
      await this.context.model('DepartmentRoles').save(departmentRole);
      const toastData: {
        Title: string;
        Message: string;
      } = this.translateService.instant('Departments.Roles.Add.SuccessToast');
      // show success toast
      this.toastService.show(toastData.Title, toastData.Message);
      // fire app event
      this.appEvent.add.next({
        model: 'DepartmentRoles'
      });
      // and close
      this.cancel();
    } catch (err) {
      console.error(err);
      this.lastError = err;
    } finally {
      // hide loading
      this.loadingService.hideLoading();
    }
  }

  onChange(data: any) {
    if (
      !(
        this.formComponent &&
        this.formComponent.form &&
        this.formComponent.form.formio &&
        typeof this.formComponent.form.formio.checkValidity === 'function'
      )
    ) {
      return;
    }
    // disable or enable ok button based on form data validity
    this.okButtonDisabled = !this.formComponent.form.formio.checkValidity(data);
  }
}
