import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, LoadingService } from '@universis/common';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-registrations-preview-latest-history',
  templateUrl: './registrations-preview-latest-history.component.html'
})
export class RegistrationsPreviewLatestHistoryComponent implements OnInit, OnDestroy {

  public registrationLogs: any[];
  public loading = true;
  private paramSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _errorService: ErrorService,
              private _loadingService: LoadingService) {
  }

  ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe(async (params) => {
      try {
        this.loading = true;
        this._loadingService.showLoading();
        this.registrationLogs = await this._context.model('ObjectEventLogs')
          .where('object').equal(params.id.toString())
          .and('objectType').equal('StudentPeriodRegistration')
          .orderByDescending('dateCreated')
          .take(-1)
          .getItems();
      } catch (err) {
        console.error(err);
        this._errorService.showError(err, {
          continueLink: '.'
        });
      } finally {
        this.loading = false;
        this._loadingService.hideLoading();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  /*
  showInfo(id: string) {
    try {
      if (!(this.registrationLogs && this.registrationLogs.length)) {
        return;
      }
      const log = this.registrationLogs.find((someLog: {id: string, title: string}) => someLog.id === id);
      if (log == null) {
        return;
      }
      return this._modalService.showDialog(this._translateService.instant('Registrations.Modifications.Description'),
       `<p>${log.title || '-'}</p>`);
    } catch (err) {
      console.error(err);
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  */
}
