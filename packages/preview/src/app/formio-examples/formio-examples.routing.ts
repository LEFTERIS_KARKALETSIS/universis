import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FormioExamplesBaseComponent} from './formio-examples-base/formio-examples-base.component'

const routes: Routes = [
  {
    path: '',
    component: FormioExamplesBaseComponent,
    data: {
      title: 'FormIO Examples'
    }
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class FormioExamplesRoutingModule {
}
